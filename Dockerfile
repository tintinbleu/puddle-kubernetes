# Use an official Python image as the base image
FROM python:3.8-slim-buster

EXPOSE 8000
# Set the working directory to /app
WORKDIR /app

# Copy the current directory contents into the container at /app
COPY . /app

# Install dependencies
RUN pip install --no-cache-dir -r requirements.txt

# Install Pillow library
RUN pip install Pillow

# Set environment variables
ENV PYTHONUNBUFFERED 1

# Run the command to start the Django development server
CMD ["python", "./puddle/manage.py", "runserver", "0.0.0.0:8000"]
